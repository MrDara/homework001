package com.example.myhomeworks;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    String TextOne,TextTwo;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextOne = getResources().getString(R.string.app_text2);
        TextTwo = getResources().getString(R.string.app_text1);
    }
    // action button
    public void Clickview1(View view) {
        Intent intent = new Intent(this,ViewActivity.class);
        intent.putExtra("image",R.drawable.view);
        intent.putExtra("text",TextOne);
        startActivityForResult(intent,1);
    }
    // action button
    public void Clickview2(View view) {
        Intent intent = new Intent(this,ViewActivity.class);
        intent.putExtra("text",TextTwo);
        intent.putExtra("image",R.drawable.view1);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check if the request code is same as what is passed  here it is 1
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                final String result = data.getStringExtra("key");
                //display it in a Toast.
                Toast.makeText(this, "Result: " + result, Toast.LENGTH_LONG).show();

            }
        }
    }
}
