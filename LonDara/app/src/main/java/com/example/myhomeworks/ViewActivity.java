package com.example.myhomeworks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewActivity extends AppCompatActivity {

    TextView textView;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        textView = findViewById(R.id.txtOne);
        textView.setText(getIntent().getStringExtra("text"));

        imageView =(ImageView) findViewById(R.id.imageView);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int resId = bundle.getInt("image");
            imageView.setImageResource(resId);
        }

    }


    public void btnSend(View view) {

        EditText editText = findViewById(R.id.etText);
        String description = editText.getText().toString();
        Intent intent = getIntent();

        intent.putExtra("key", description);
        setResult(RESULT_OK, intent);
        finish();

    }


}